#!/bin/bash

#安裝基礎套件
yum -y install epel-release
yum -y install vim net-tools telnet htop wget git zip unzip bind-utils bash-completion

#關閉系統防火牆與Selinux
systemctl stop firewalld
systemctl disable firewalld
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

#建立下載目錄
systemctl stop firewalld
systemctl disable firewalld
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

#建立下載目錄
mkdir -p /data/dl

#安裝Nginx
wget http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
rpm -ivh nginx-release-centos-7-0.el7.ngx.noarch.rpm
yum install nginx -y

#啟動Nginx
systemctl start nginx

#設定開機啟動Nginx
systemctl enable nginx

#安裝Mysql
rpm -Uvh https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
sed -i 's/gpgcheck=1/gpgcheck=0/g' /etc/yum.repos.d/mysql-community.repo
yum install mariadb-server -y

#啟動mysql並設定開機預設啟動
systemctl start mysqld
systemctl enable mysqld


#安裝PHP
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum install php72w php72w-devel php72w-fpm php72w-gd php72w-mbstring php72w-mysql php72w-bcmath php72w-xml php72w-cli php72w-common php72w-ldap -y
sed -i 's/user = apache/user = nginx/g' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = nginx/g' /etc/php-fpm.d/www.conf


#配置PHP

sed -i 's/expose_php = On/expose_php = Off/g' /etc/php.ini
sed -i 's/max_execution_time = 30/max_execution_time = 300/g' /etc/php.ini
sed -i 's/max_input_time = 60/max_input_time = 300/g' /etc/php.ini
sed -i 's/post_max_size = 8M/post_max_size = 16M/g' /etc/php.ini
sed -i '800i always_populate_raw_post_data = -1' /etc/php.ini
sed -i 's/;date.timezone =/date.timezone = Asia\/Taipei/g' /etc/php.ini

cp /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.backup
cat > /etc/nginx/conf.d/default.conf << "EOF"
server {
    listen       80;
    server_name  localhost;

   

    location / {
        root   /usr/share/nginx/html;
        index  index.php index.html index.htm;
    }

    
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

   
    
    location ~ \.php$ {
        root           /usr/share/nginx/html;
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
    }

}
EOF
nginx -s reload
systemctl start php-fpm.service



yum -y install gcc gcc-c++ unixODBC-devel mysql-devel net-snmp-devel libevent libevent-devel libxml2-devel libcurl libcurl-devel java-1.6.0-openjdk-devel fping curl-devel libxml2 snmpd net-snmp


useradd -U zabbix


cd /data/dl

wget https://cdn.zabbix.com/zabbix/sources/stable/5.0/zabbix-5.0.19.tar.gz


tar -zxvf zabbix-5.0.19.tar.gz


cd /data/dl/zabbix-5.0.19



./configure --prefix=/data/zabbix-server --enable-server --enable-agent --enable-proxy --with-mysql=/usr/bin/mysql_config --with-net-snmp --with-libcurl --with-libxml2 --with-unixodbc --enable-java

make && make install

cd /data/dl/zabbix-5.0.19/database/mysql

#搜索以下幾個字段修改
sed -i 's/# DBPassword=/DBPassword=Zabbix0000!/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# DBSocket=/DBSocket=\/var\/lib\/mysql\/mysql.sock/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# DBPort=/DBPort=3306/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/LogFile=\/tmp\/zabbix_server.log/LogFile=\/data\/zabbix-server\/zabbix_server.log/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# LogFileSize=1/LogFileSize=0/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# PidFile=\/tmp\/zabbix_server.pi/PidFile=\/data\/zabbix-server\/zabbix_server.pid/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# SocketDir=\/tmp/SocketDir=\/data\/zabbix/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# SNMPTrapperFile=\/tmp\/zabbix_traps.tmp/SNMPTrapperFile=\/data\/snmptrap/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# AlertScriptsPath=${datadir}\/zabbix\/alertscripts/AlertScriptsPath=\/data\/zabbix-server\/share\/zabbix\/alertscripts/g' /data/zabbix-server/etc/zabbix_server.conf
sed -i 's/# ExternalScripts=${datadir}\/zabbix\/externalscripts/ExternalScripts=\/data\/zabbix-server\/share\/zabbix\/externalscripts/g' /data/zabbix-server/etc/zabbix_server.conf

mkdir -p /data/zabbix
mkdir -p /data/snmptrap

#配置zabbix-server前端界面
mkdir -p /usr/share/nginx/html/zabbix
cd /data/dl/zabbix-5.0.19/ui
cp -a * /usr/share/nginx/html/zabbix/

#變更目錄權限
chown -R zabbix:zabbix /data/zabbix-server
chown -R zabbix:zabbix /data/zabbix
chown -R zabbix:zabbix /data/snmptrap
chown -R zabbix:zabbix /usr/share/nginx/
chmod -R 777 /var/lib/php/session/

#修改Zabbix-Agent配置
cp /data/zabbix-server/etc/zabbix_agentd.conf /data/zabbix-server/etc/zabbix_agentd.conf.backup
sed -i 's/# UnsafeUserParameters=0/UnsafeUserParameters=1/g' /data/zabbix-server/etc/zabbix_agentd.conf






echo "===================================================================="
echo "基本服務都安裝完成 以下須手動執行"
echo "cat /var/log/mysqld.log | grep 'temporary password'"
echo "使用初始化密碼登入DB"
echo "alter user 'root'@'localhost' identified by 'Ec78tVzMmxwV2Fa!&';"
echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'Ec78tVzMmxwV2Fa!&';"
echo "create database zabbix character set utf8 collate utf8_bin;"
echo "grant all privileges on *.* to 'zabbix'@'%' identified by 'Ec78tVzMmxwV2Fa!&';"
echo "flush privileges;"
echo "cd /data/dl/zabbix-5.0.19/database/mysql"
echo "mysql -u zabbix -p zabbix < schema.sql"
echo "mysql -u zabbix -p zabbix < images.sql"
echo "mysql -u zabbix -p zabbix < data.sql"
echo "/data/zabbix-server/sbin/zabbix_server -c /data/zabbix-server/etc/zabbix_server.conf"
echo "/data/zabbix-server/sbin/zabbix_agentd -c /data/zabbix-server/etc/zabbix_agentd.conf"
echo "systemctl restart php-fpm.service"
echo "systemctl restart nginx"



